<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Articulo;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ArticuloController extends AbstractController
{

    /**
     * @Route("/listado_articulos", name="listado-articulos")
     */
    public function index()
    {
        $repository = $this->getDoctrine()->getRepository(Articulo::class);
        $articulos = $repository->findBy([],["numero" => 'ASC']);
        //if (empty($articulos)) {throw new NotFoundHttpException('No hay articulos');}
        return $this->render('articulo/listado_articulos.html.twig', ['articulos' => $articulos]);
    }
}