<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Articulo;
use App\Entity\Movimiento;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class MovimientoController extends AbstractController
{
    /**
     * @Route("/listado_movimientos", name="listado-movimientos")
     */
    public function index()
    {
        $repository = $this->getDoctrine()->getRepository(Movimiento::class);
        $movimientos = $repository->findAll();
        /*if (is_null($movimientos)) {
            throw new \Exception("No existen Movimientos", 201);
        }*/
        return $this->render('movimiento/listado_movimientos.html.twig', ['movimientos' => $movimientos]);
    }


    /**
     * @Route("/listado_movimientos/articulo/{id}", name="listado-movimientos-por-id")
     */
    public function show(Request $request, String $id)
    {
        $repository = $this->getDoctrine()->getRepository(Articulo::class);
        $articulo = $repository->findOneBy(['id' => $id ]);
        if (empty($articulo)) {
            //throw new \Exception("El articulo solicitado no existe", 404);
            throw new NotFoundHttpException('Artículo Inexistente');
        }
        $movimientos = $articulo->getMovimientos();
        return $this->render('movimiento/listado_movimientos.html.twig', ['movimientos' => $movimientos]);
    }
}