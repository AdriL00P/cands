<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Articulo;
use App\Entity\Movimiento;
use App\Repository\ArticuloRepository;
use App\Repository\MovimientoRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use DateTime;



class ArticuloAPIController extends AbstractController
{
    private $articuloRepository;

    public function __construct(ArticuloRepository $articuloRepository)
    {
        $this->articuloRepository = $articuloRepository;
    }
    /**
     * @Route("/api/articulos", name="api_listado-articulos", methods={"GET"})
     */
    public function index()
    {
        $articulos = $this->articuloRepository->findAll();
        $data = [];

        if (empty($articulos)) {
            return new JsonResponse(['status' => '404'], Response::HTTP_NOT_FOUND);
            //throw new \Exception("El articulo solicitado no existe", 404);
            //throw new NotFoundHttpException('Artículo Inexistente');
        }



        foreach ($articulos as $articulo) {
            $data[] = [
                'id' => $articulo->getId(),
                'numero' => $articulo->getNumero(),
                'descripcion' => $articulo->getDescripcion(),
                'inventario' => $articulo->getInventario(),
                'ubicacion' => $articulo->getUbicacion(),  
            ];
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }


    /**
     * @Route("/api/articulos/{id}", name="api_detalle-articulo", methods={"GET"})
     */
    public function show(Request $request, String $id)
    {
        $articulo = $this->articuloRepository->findOneBy(['id' => $id]);

        if (empty($articulo)) {
            return new JsonResponse(['status' => '404'], Response::HTTP_NOT_FOUND);
        }
        
        $data = [
                'id' => $articulo->getId(),
                'numero' => $articulo->getNumero(),
                'descripcion' => $articulo->getDescripcion(),
                'inventario' => $articulo->getInventario(),
                'ubicacion' => $articulo->getUbicacion(),  
        ];

        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("/api/articulos/{id}/movimientos", name="api_movimientos-articulo", methods={"GET"})
     */
    public function show_movimientos(Request $request, String $id)
    {
        $articulo = $this->articuloRepository->findOneBy(['id' => $id]);
        
        if (empty($articulo)) {
            return new JsonResponse(['status' => '404'], Response::HTTP_NOT_FOUND);
        }

        $movimientos = $articulo->getMovimientos();

        // Se podría validar $movimientos ESTA VACIO para personalizar la respuesta

        $data = [];

        foreach ($movimientos as $movimiento) {
            $data[] = [
                'id' => $movimiento->getId(),
                'cantidad' => $movimiento->getCantidad(),
                'tipo' => $movimiento->getTipo(),
                'fecha' => $movimiento->getFecha(),
                'articulo_referencia' => [
                    'id' => $articulo->getId(),
                    'numero' => $articulo->getNumero(),
                    'descripcion' => $articulo->getDescripcion(),
                    'inventario' => $articulo->getInventario(),
                    'ubicacion' => $articulo->getUbicacion(), 
                ]
            ];
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }

    private function registrarMovimiento(String $cantidad, String $tipoMovimiento, Articulo $articulo ){
        $nueva_cantidad = $cantidad;
        if ($tipoMovimiento != 'ajuste'){
            $nueva_cantidad = $tipoMovimiento == 'compra' ? $articulo->getInventario()+$cantidad : $articulo->getInventario()-$cantidad;
        }
        // Actualizo el inventario con el nuevo total, dependiendo de la operación
        $articulo->setInventario($nueva_cantidad);

        $entityManager = $this->getDoctrine()->getManager();

        $nuevoMovimiento = new Movimiento();
        // Asigno al movimiento la cantidad correspondiente a la operación actual
        $nuevoMovimiento->setCantidad($cantidad);
        $nuevoMovimiento->setTipo($tipoMovimiento);
        $nuevoMovimiento->setFecha(new DateTime('now'));
        $nuevoMovimiento->setArticulo($articulo);

        $entityManager->persist($nuevoMovimiento);
        return $entityManager->flush();
    }


    /**
     * @Route("/api/articulos/{id}/compra", name="api_compra-articulo", methods={"POST"})
     */
    public function comprar(Request $request, String $id)
    {
        $json_data =  $request->getContent();      
        if (empty($json_data)) {return new JsonResponse(['status' => 'Empty body json request data'], Response::HTTP_BAD_REQUEST);}
        
        $articulo = $this->articuloRepository->findOneBy(['id' => $id]);
        if (empty($articulo) || empty($id) ) {return new JsonResponse(['status' => 'ID de Artículo inválido'], Response::HTTP_BAD_REQUEST);}

        $data = json_decode($json_data, true);
        if (array_key_exists('cantidad', $data)) {$cantidad = $data['cantidad'];}
        else{return new JsonResponse(['status' => 'Datos incompletos.'], Response::HTTP_BAD_REQUEST);}
        if(!is_numeric($cantidad)){return new JsonResponse(['status' => 'Cantidad debe ser un número'], Response::HTTP_BAD_REQUEST);}

        $this->registrarMovimiento($cantidad,'compra',$articulo);
        return new JsonResponse(['status' => 'Compra Registrada'], Response::HTTP_CREATED);
    }

    /**
     * @Route("/api/articulos/{id}/venta", name="api_venta-articulo", methods={"POST"})
     */
    public function vender(Request $request, String $id)
    {
        $json_data =  $request->getContent();      
        if (empty($json_data)) {return new JsonResponse(['status' => 'Empty body json request data'], Response::HTTP_BAD_REQUEST);}
        
        $articulo = $this->articuloRepository->findOneBy(['id' => $id]);
        if (empty($articulo) || empty($id) ) {return new JsonResponse(['status' => 'ID de Artículo inválido'], Response::HTTP_BAD_REQUEST);}

        $data = json_decode($json_data, true);
        if (array_key_exists('cantidad', $data)) {$cantidad = $data['cantidad'];}
        else{return new JsonResponse(['status' => 'Datos incompletos.'], Response::HTTP_BAD_REQUEST);}
        if(!is_numeric($cantidad)){return new JsonResponse(['status' => 'Cantidad debe ser un número'], Response::HTTP_BAD_REQUEST);}

        $this->registrarMovimiento($cantidad,'venta',$articulo);
        return new JsonResponse(['status' => 'Venta Registrada'], Response::HTTP_CREATED);
    }

    /**
     * @Route("/api/articulos/{id}/ajuste", name="api_ajuste-articulo", methods={"PATCH"})
     */
    public function ajustar(Request $request, String $id)
    {
        $json_data =  $request->getContent();      
        if (empty($json_data)) {return new JsonResponse(['status' => 'Empty body json request data'], Response::HTTP_BAD_REQUEST);}
        
        $articulo = $this->articuloRepository->findOneBy(['id' => $id]);
        if (empty($articulo) || empty($id) ) {return new JsonResponse(['status' => 'ID de Artículo inválido'], Response::HTTP_BAD_REQUEST);}

        $data = json_decode($json_data, true);
        if (array_key_exists('cantidad', $data)) {$cantidad = $data['cantidad'];}
        else{return new JsonResponse(['status' => 'Datos incompletos.'], Response::HTTP_BAD_REQUEST);}
        if(!is_numeric($cantidad)){return new JsonResponse(['status' => 'Cantidad debe ser un número'], Response::HTTP_BAD_REQUEST);}

        $this->registrarMovimiento($cantidad,'ajuste',$articulo);
        return new JsonResponse(['status' => 'Ajuste Registrado'], Response::HTTP_CREATED);
    }

}