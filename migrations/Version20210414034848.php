<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210414034848 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE articulo (id INT AUTO_INCREMENT NOT NULL, numero INT NOT NULL, descripcion VARCHAR(255) NOT NULL, inventario INT NOT NULL, ubicacion VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE movimiento (id INT AUTO_INCREMENT NOT NULL, articulo_id INT NOT NULL, cantidad INT NOT NULL, tipo VARCHAR(20) NOT NULL, fecha DATETIME NOT NULL, INDEX IDX_C8FF107A2DBC2FC9 (articulo_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE movimiento ADD CONSTRAINT FK_C8FF107A2DBC2FC9 FOREIGN KEY (articulo_id) REFERENCES articulo (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE movimiento DROP FOREIGN KEY FK_C8FF107A2DBC2FC9');
        $this->addSql('DROP TABLE articulo');
        $this->addSql('DROP TABLE movimiento');
    }
}